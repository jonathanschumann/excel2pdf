import win32com.client as win32
import os

def openWorkbook(xlapp, xlfile):
    try:        
        xlwb = xlapp.Workbooks(xlfile)            
    except Exception as e:
        try:
            xlwb = xlapp.Workbooks.Open(xlfile)
        except Exception as e:
            print(e)
            xlwb = None                    
    return(xlwb)

try:
    print("Place the Excel File in the same Folder as this script")
    file = input("Enter the filename of the Excel File you want to convert to PDFs:")
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    wb = openWorkbook(excel, os.getcwd() + '\\' + file)
    for ws in wb.Worksheets:
        ws.PageSetup.Zoom = False
        ws.PageSetup.FitToPagesTall = False
        ws.PageSetup.FitToPagesWide = 1
        ws.PageSetup.LeftMargin = 25
        ws.PageSetup.RightMargin = 25
        ws.PageSetup.TopMargin = 50
        ws.PageSetup.BottomMargin = 50
        ws.ExportAsFixedFormat(0,os.getcwd() + '\\' + file + '_' + ws.Name + '.pdf')
    wb.Close(True)
except Exception as e:
    print(e)

finally:
    wb = None
    excel = None